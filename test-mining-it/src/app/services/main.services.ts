import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})

export class MainService {
  public numbersApi = 'http://patovega.com/prueba_frontend/array.php';
  public paragraphApi = 'http://patovega.com/prueba_frontend/dict.php';

  constructor(private http: HttpClient) { }

  public getNumbers(): Observable<any> {
    return this.http.get<any>(this.numbersApi)
      .pipe(catchError(this.errorHandler));
  }

  public getParagraph(): Observable<any> {
    return this.http.get<any>(this.paragraphApi)
      .pipe(catchError(this.errorHandler));
  }

  public errorHandler(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      console.error('Ocurrió un error:', error.error.message);
    } else {
      console.error(
        `La API retornó: ${error.status}`);
    }

    return throwError(
      'Intente en unos minutos :(');
  }
}
