import { Component } from '@angular/core';
import { MainService } from 'src/app/services/main.services';

@Component({
  selector: 'app-paragraph',
  templateUrl: 'paragraph.component.html',
  styleUrls: ['./paragraph.component.scss']
})

export class ParagraphComponent {
  // declaro variables a usar
  public  paragraphData: any;
  public alphabet = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'n', 'ñ', 'o', 'p', 'q', 'r',
    's', 't', 'u', 'v', 'w', 'x', 'y', 'z'];
  public letter: any;
  public finalInfoArray: any = [];
  public auxArray: any = [];
  public loading: boolean;

  constructor(private mainService: MainService) { }

  public getInfo(): void {
    this.loading = true;
    // llamo al servicio que me trae los datos de la api
    this.mainService.getParagraph().subscribe(data => {
      this.paragraphData = JSON.parse(data.data);
      this.countLetters(this.paragraphData);
    });
  }

  public countLetters(array): void {
    this.finalInfoArray = [];
    this.letter = 0;
    // recorro el array y obtengo las letras repetidas por cada parrafo
    for (let i = 0; i < array.length; i++) {
      for (let x = 0; x < this.alphabet.length; x++) {
        this.letter = array[i].paragraph.split(this.alphabet[x]).length - 1;
        // primero armo un array con las letras repetidas del parrafo indice i
        this.auxArray.push({
          letra: this.letter
        });
      }

      // luego pusheo el array anterior con el conteo de letras
      // por parrafo y lo asigno a un nuevo array.
      this.finalInfoArray.push({
        letras: this.auxArray,
        number: Math.abs(array[i].number)
      });

      this.loading = false;

      this.auxArray = [];
    }
  }
}
