import { Component } from '@angular/core';
import { MainService } from 'src/app/services/main.services';

@Component({
  selector: 'app-numbers',
  templateUrl: 'numbers.component.html',
  styleUrls: ['./numbers.component.scss']
})

export class NumbersComponent {
  // declaro variables a usar.
  public numberData: any;
  public newArray: any;
  public repetitions: number;
  public lastAparition = 0;
  public orderLegacyArray: any = [];
  public loading: boolean;

  constructor(private mainService: MainService) { }

  public getNumbersData(): any {
    this.loading = true;
    // llamo al servicio que trae los datos de la api
    this.mainService.getNumbers().subscribe(data => {
      this.numberData = data.data;
      this.setNewArray(this.numberData);
      this.orderNumbers(this.numberData);
    });
  }

  public setNewArray(array): any {
    this.newArray = [];
    // recorro el array para obtener las repeticiones
    // y el ultimo indice de aparicion.
    for (let i = 0; i < array.length; i++) {
      this.repetitions = 0;
      for (let x = 0; x < array.length; x++) {
        if (array[i] === array[x]) {
          this.repetitions ++;
          this.lastAparition = x;
        }
      }

      // pusheo los datos al nuevo array
      this.newArray.push({
        number: array[i],
        quantity: this.repetitions,
        first: [i],
        last: this.lastAparition
      });
    }

    // elimino los duplicados
    this.newArray = this.newArray.filter(function(a) {
      return !this[a.number] && (this[a.number] = true);
    }, Object.create(null));

    this.loading = false;
  }

  public orderNumbers(orderedArray): void {
    // ordeno el array obtenido en el primer metodo (array principal)
    // de forma ascendente
    for (let i = 1; i < orderedArray.length; i++) {
      const tmp = orderedArray[i];
      for (var j = i - 1; j >= 0 && (orderedArray[j] > tmp); j--) {
        orderedArray[j + 1] = orderedArray[j];
      }
      orderedArray[j + 1] = tmp;
    }

    this.orderLegacyArray = orderedArray.toString();
  }
}
