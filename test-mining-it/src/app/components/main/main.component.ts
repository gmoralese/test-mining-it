import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-main',
  templateUrl: 'main.component.html',
  styleUrls: ['./main.component.scss']
})

export class MainComponent implements OnInit {
  public componentToShow = true;

  constructor() { }

  public ngOnInit() {}

  public changeView(view: number): any {
    if (view === 2) {
      this.componentToShow = false;
    } else {
      this.componentToShow = true;
    }
  }
}
