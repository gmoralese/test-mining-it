import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MainComponent } from './main/main.component';
import { NumbersComponent } from './numbers/numbers.component';
import { ParagraphComponent } from './paragraph/paragraph.component';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [MainComponent, NumbersComponent,
    ParagraphComponent],
  imports: [CommonModule, FormsModule],
  exports: [MainComponent, NumbersComponent,
    ParagraphComponent]
})

export class ComponentsModule {}
